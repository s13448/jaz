package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Rata;

public class HelloServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	
	ArrayList<Rata> list = new ArrayList<Rata>();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
//		response.getWriter().println("<h1>Hello World</h1>");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String title = "Schedulde account";
	      String docType =
	         "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

	      out.println(docType +
	         "<html>\n" +
	         "<head><title>" + title + "</title></head>\n" +
	         "<body bgcolor = \"#f0f0f0\">\n" +
	         "<h1 align = \"center\">" + title + "</h1>\n" +
	         "<table width = \"100%\" border = \"1\" align = \"center\">\n" +
	         "<tr bgcolor = \"#949494\">\n" +
	         "    <th>Nr raty</th>" + 
				"    <th>Kwota Kapitalu</th> " + 
				"    <th>Kwota odsetek</th>" + 
				"    <th>Oplaty stale</th>" + 
				"    <th>Calkowita kwota raty</th>" + 
				"  </tr>" + 
				showTable(list)
				+"</table>"
	      );
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		 String kwota = request.getParameter("kwota");
		 String iloscRat = request.getParameter("iloscRat");
		 String procent = request.getParameter("procent");
		 String stala = request.getParameter("stala");
		 String rodzajRaty = request.getParameter("rodzajRaty");
		 
		 if(kwota == null | kwota.equals(""))
		{
			response.sendRedirect("/test.jsp");
		} else if (Integer.parseInt(rodzajRaty) == 1) {
				rataMalejaca(kwota, iloscRat, procent, stala);
			}
		else if(Integer.parseInt(rodzajRaty) == 2) {
				rataStala(kwota, iloscRat, procent, stala);
		}
			
		response.getWriter().println("<h1>Hello World</h1>");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String title = "Reading All Form Parameters";
	      String docType =
	         "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
	      	      
	      out.println(docType +
	 	         "<html>\n" +
	 	         "<head><title>" + title + "</title></head>\n" +
	 	         "<body bgcolor = \"#f0f0f0\">\n" +
	 	         "<h1 align = \"center\">" + title + "</h1>\n" +
	 	         "<table width = \"100%\" border = \"1\" align = \"center\">\n" +
	 	         "<tr bgcolor = \"#949494\">\n" +
	 	         "    <th>Nr raty</th>" + 
	 				"    <th>Kwota Kapitalu</th> " + 
	 				"    <th>Kwota odsetek</th>" + 
	 				"    <th>Oplaty stale</th>" + 
	 				"    <th>Calkowita kwota raty</th>" + 
	 				"  </tr>"  + 
	 				showTable(list)
	 				+"</table>"
	 	      );
		}
	
	public String showTable(ArrayList<Rata> listTable) {
		System.out.println(listTable);
		String tmp= "";
		for(int i = 0; i<listTable.size(); i++) {
			Rata rt = (Rata) listTable.get(i);
			tmp = tmp + "<tr>" +
					"<td>" + rt.nrRaty + "</td>" +
	    		    "<td>" + rt.kwotaMalejaca + "</td>" +
	    		    "<td>" + rt.odsetki+ "</td>" +
	    		    "<td>" + rt.oplataStala + "</td>" +
	    		    "<td>" + rt.rata + "</td>" + "</tr>";
	      }
	      
		
		return tmp;
	
	}
	
public void rataMalejaca(String kwota, String iloscRat, String procent, String stala)
{		
		float czescKapitalowa = Integer.parseInt(kwota)/Integer.parseInt(iloscRat);
		float kwotaZmniejszajacaSie = Integer.parseInt(kwota);
		int numerRaty = 1;
		float rata;
		float czescOdsetkowa;

		for(int i=0; i<Integer.parseInt(iloscRat); i++) {

			czescOdsetkowa = kwotaZmniejszajacaSie * (Float.parseFloat(procent)/100)/12;
			rata =  czescKapitalowa + czescOdsetkowa + Integer.parseInt(stala);
			
			Rata rat = new Rata(numerRaty,rata,kwotaZmniejszajacaSie,czescOdsetkowa, stala);
			list.add(rat);
			kwotaZmniejszajacaSie = kwotaZmniejszajacaSie - rata;
			numerRaty++;
			
		}
	}

public void rataStala(String kwota, String iloscRat, String procent, String stala)
{
	new ArrayList<Rata>();		
	float kwotaZmniejszajacaSie = Integer.parseInt(kwota);
	int numerRaty = 1;
	float rata;
	float czescOdsetkowa;
	float q = 1 + ((Float.parseFloat(procent)/100)/12);
	float temp = (float) Math.pow(q, Integer.parseInt(iloscRat));
	
	czescOdsetkowa =  Float.parseFloat(kwota) * temp * ((q	-1)/(temp-1)) - (kwotaZmniejszajacaSie/Integer.parseInt(iloscRat));
	rata = czescOdsetkowa + Float.parseFloat(stala);	

	for(int i=0; i<Integer.parseInt(iloscRat); i++) {
		Rata rt = new Rata(numerRaty,rata,kwotaZmniejszajacaSie,czescOdsetkowa, stala);
		list.add(rt);
		numerRaty++;
		
	}	
}

}
